 # formPrint.py - print formatter
 # andyp - 21.05.18

formatter = "%r %r %r %r"

print formatter % (1,2,3,4)
print formatter % ("one","two","three","four")
print formatter % (True,False,False,True)
print formatter % (formatter,formatter,formatter,formatter)
print formatter % ("Not in this life",
                   "The mupppet said to the thief",
                   "but the donkey left",
                   "And the raven cried"
                   )
