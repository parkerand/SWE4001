# moPrint.py -- advanced printing code
# andyp 16.05.18

print("Mary had a little lamb.")
print("Its fleece was as white as %s." % 'snow')
print("And she Wwas very hungry.")
print("," * 10) # erm what will thisdo

end1 = "D"
end2 = "o"
end3 = "n"
end4 = "a"
end5 = "r"
end6 = "K"
end7 = "e"
end8 = "b"
end9 = "a"
end10 = "b"

# watach the comma at the end what happens if you remove it
print (end1 + end2 + end3 + end4 + end5,)
print (end6 + end7 + end8 + end9 + end10)
