#commentProg.py
#andyp 01.03.18

# This is a comment it tells me what I have done
# another comment

print ("Inline Comment") # and all this is ignored

#comments can also disable code
# print ("Not running today")

print ("Not commented so running like the wind !!")
