# me.py 14.05.18
# variables and test printing example

my_name = "Winston Grimlett"
my_age = 42 #  real
my_height =  1.78 #  metres
my_weight = 78 #  kgs
my_eyes = "Grey"
my_teeth = "Yellow"
my_hair = "Black"

print("Let's talk about %s." % my_name)
print("He's %d metres tall." % my_height)
print("He's %d kilos heavy." % my_weight)
print("Actually that's a little light.")
print("He has %s eyes and %s hair." %(my_eyes, my_hair))
