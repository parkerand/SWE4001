## taxnTips.py
## calculate vat and tips for a meal
## 21.03.17

meal_cost = float(input('Enter the meal cost: '))

tax = meal_cost *.2
meal = meal_cost - tax
tip = meal_cost *.18
total = tax + meal + tip

print(f'meal price is    {meal: .2f}')
print(f'meal tax is       {tax: .2f}')
print(f'meal tip is       {tip: .2f}')
print( '=======================')
print(f'total meal price {total: .2f}')
print( '=======================')
